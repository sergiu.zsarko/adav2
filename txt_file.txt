
    main_file
        --> java.lang.Object *
        main(java.lang.String[])
            --> java.io.FileReader *
            --> java.io.FileReader.FileReader(java.lang.String) *
            --> java.io.FileReader.close() *
            --> java.io.InputStream *
            --> java.io.PrintStream *
            --> java.io.PrintStream.println(java.lang.String) *
            --> java.lang.Exception *
            --> java.lang.Exception.getMessage() *
            --> java.lang.Readable *
            --> java.lang.String *
            --> java.lang.String[] *
            --> java.lang.System.in *
            --> java.lang.System.out *
            --> java.util.Scanner *
            --> java.util.Scanner.Scanner(java.io.InputStream) *
            --> java.util.Scanner.Scanner(java.lang.Readable) *
            --> java.util.Scanner.close() *
            --> java.util.Scanner.hasNextLine() *
            --> java.util.Scanner.next() *
            --> java.util.Scanner.nextLine() *
        main_file()
            --> java.lang.Object.Object() *
java.io *
    FileReader *
        <-- main_file.main(java.lang.String[])
        FileReader(java.lang.String) *
            <-- main_file.main(java.lang.String[])
        close() *
            <-- main_file.main(java.lang.String[])
    InputStream *
        <-- main_file.main(java.lang.String[])
    PrintStream *
        <-- main_file.main(java.lang.String[])
        println(java.lang.String) *
            <-- main_file.main(java.lang.String[])
java.lang *
    Exception *
        <-- main_file.main(java.lang.String[])
        getMessage() *
            <-- main_file.main(java.lang.String[])
    Object *
        <-- main_file
        Object() *
            <-- main_file.main_file()
    Readable *
        <-- main_file.main(java.lang.String[])
    String *
        <-- main_file.main(java.lang.String[])
    String[] *
        <-- main_file.main(java.lang.String[])
    System *
        in *
            <-- main_file.main(java.lang.String[])
        out *
            <-- main_file.main(java.lang.String[])
java.util *
    Scanner *
        <-- main_file.main(java.lang.String[])
        Scanner(java.io.InputStream) *
            <-- main_file.main(java.lang.String[])
        Scanner(java.lang.Readable) *
            <-- main_file.main(java.lang.String[])
        close() *
            <-- main_file.main(java.lang.String[])
        hasNextLine() *
            <-- main_file.main(java.lang.String[])
        next() *
            <-- main_file.main(java.lang.String[])
        nextLine() *
            <-- main_file.main(java.lang.String[])
