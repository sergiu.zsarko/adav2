//Kosaraju Algorithm:
//calc dfs on the graph G, compute finish time u,f for every vertex
//compute Gt, the reverse of graph G
//compute dfs on Gt but in main loop, consider verticises in decreasing order of u,f compared to normal;
//Output vertices of each DFS-tree formed in step 3 as strongly connected components.

import java.io.FileReader;
import java.util.*;



class node{
    String node_name;//name of the class
    Set<String> dependencies;//the list of dependencies it goes towards
    node(String node_name)
    {
        dependencies=new HashSet<>();
        this.node_name=node_name;
    }
    public void add_dependency(String dep)
    {
        dependencies.add(dep);
    }
    public String toString()
    {
        String s=node_name+":";
        for(String k:dependencies)
        {
            s+=k+"->";
        }
        //this is just so that we get rid of the final "->" which points to nothing usually
        //ex: From: "ClassA=ClassB->ClassC->" To:"ClassA=ClassB->ClassC"
        return s.substring(0, s.length()-2);
    }
}

class direct_graph
{

    Set<node> adj_list;//the adjacency list of nodes.

    direct_graph()
    {
        adj_list=new HashSet<>();
        init_from_file();
    }

    public String get_dependency(int start,String name,String line_content)
    {
        //java dependency; won't be taken into account.
        if(line_content.charAt(line_content.length()-1)=='*')
        {
            return "";
        }
        //dealing with dependencies.
        //classA -> classB case
        // line_content.trim();
        if(line_content.charAt(start)=='-' && line_content.charAt(start+1)=='-' && line_content.charAt(start+2)=='>')
        {
        
            int cnt=start+4;
            while( cnt<line_content.length() && line_content.charAt(cnt)!='.' )
            {
                cnt++;
            }
            String class_dep_name=line_content.substring(start+4, cnt).trim();
            if(!name.equals(class_dep_name))
                return class_dep_name;
        }
        
        return "";
    }

    void init_from_file()
    {
        Scanner s=new Scanner(System.in);
        System.out.println("Give the name of the file:");
        String source=s.next();
        s.close();
        try
        {
            FileReader f=new FileReader(source);
            Scanner file_scanner=new Scanner(f);
            //skip the first line since its blank always
            file_scanner.nextLine();
            String name="";//its not dangerous to set the name of a class to blank as a base value since the first value we will have in our text file will be the class
                           //name and following that we will get its dependencies. We will NEVER have the dependencies before the class name.
            while(file_scanner.hasNextLine())
                {
                    //start building the graph.
                    
                    String line_content=file_scanner.nextLine();
                    
                    //if we dont have a space at our first index, then our Dependency Extractor is starting to write about the already predefinied java classes.
                    //we are not interested in those so we stop reading from the file once we get to that point.
                    if(line_content.charAt(0)!=' ')
                        {
                            break;
                        }
                    //we can see if a line in our code represents a class name if there are 4 spaces before its name.
                    // we can also simplify this since we have 3 cases in our txt file: either there are 4 spaces, more than 4 spaces or no spaces.
                    //because of that, we can just test if the first character is a space and if there is no space at the 5th character
                    if(line_content.charAt(0)==' ' && line_content.charAt(4)!=' ')
                    {
                        //we found a class so we trim it(remove the white spaces before/after) and add it to our adj list with that name.
                        name=line_content.trim();
                        adj_list.add(new node(name));
                    }
                    else
                    {
                        if(line_content.length()>12 && (line_content.charAt(12)=='<' || line_content.charAt(12)=='-'))
                        {
                            //get dependencies from the method
                            String depen=get_dependency(12, name, line_content);

                            //if the dependency was invalid(ex: java dependency,same name as base class etc)
                            if(depen=="")
                                continue;
                            
                            //if valid, search through our list to find the class that matches the current class name.
                            for(node n:adj_list)
                            {
                                //if found, add the dependency with that name.
                                if(n.node_name.equals(name))
                                {
                                    n.add_dependency(depen);
                                }
                            }
                        }
                        else
                        {
                            //get base dependencies
                            String depen=get_dependency(8, name, line_content);
                            //The explanation for the bellow code is the same as for the code above where we get the dependencies from the method instead of base.
                            
                            if(depen=="")
                                continue;

                            for(node n:adj_list)
                            {
                                if(n.node_name.equals(name))
                                {
                                    n.add_dependency(depen);
                                }
                            }
                        }
                    }
                }
            file_scanner.close();
            f.close();
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
    public void show_dependencies()
    {
        for(node n: adj_list)
        {
            System.out.println(n.toString());
        }
    }

    public String Kosaraju()
    {
        String s="";//placeholder value that will return if we have any dangerous cycles
        Stack<String> stack=new Stack<>();//holds finishing times of the dfs for each node
        HashMap<String,Boolean> visited=new HashMap<>();//removed dfs helper method
        List<Set<String>> scc_list=new ArrayList<>();//here we will have our strongly connected components.

        //STEP 1: COMPUTE THE FINISHING TIMES FOR NORMAL MAP.
        for(node n: adj_list)
        {
            //here you can also write !visited.containsKey(node.name) and put the dfs in that conditional.
            if(visited.containsKey(n.node_name))
                continue;
            dfs(n.node_name,visited,stack);
        }
        //now we have some values in the stack and visited hashmap. The values in the stack represents the finishing time of the nodes and the visited map will contain
        //all the nodes that have been visited.

        //reset the visited map.
        visited.clear();

        //STEP 2: COMPUTE THE REVERSE/TRANSPOSE OF THE GRAPH
        Set<node> reverse_adj_list=reverse_graph();

        //STEP 3: STARTING FROM TOP OF STACK CONTAINING OUR FINISHING TIMES, FIND SCC
        
        //while we still have nodes in the stack(node names, not nodes)
        while(!stack.isEmpty())
        {
            //get the value of the node name from the stack
            String node_explored=stack.pop();
            if(visited.containsKey(node_explored))
                continue;
            //declare a new variable that will hold our singular strongly connected component
            Set<String> comp=new HashSet<>();

            //compute dfs on the reversed graph
            dfs(node_explored,visited,comp,reverse_adj_list);

            //add the strongly connected component to the list holding all of them.
            scc_list.add(comp);
        }
        
        
        //STEP 4:IDENTIFY SCC THAT CONTAIN MORE THAN 2 NODES:
        int flag=0;
        for(Set<String> set:scc_list)
        {
            if(set.size()>2)
                {
                    s+="\n";
                    if(flag==0)
                        s+="There are problems with circular dependencies in following groups of classes:\n";
                    flag=1;
                    
                    for(String m: set)
                    {
                        s+=m+",";
                    }
                }
        }
        if(flag==1)
        {
            s=s.substring(0,s.length()-1);
        }
        if(s=="")
            {
                s+="There are no dangerous cycles in our program.";
            }
        return s;
    }
    private void dfs(String start_class,HashMap<String,Boolean> visited,Stack<String> stack)
    {
        //mark the starting class as visited.
        visited.put(start_class,true);
        //find the node that has the name of the start class
        for(node n:adj_list)
        {
            //found node
            if(n.node_name.equals(start_class))
            {
                //iterate through its dependencies:
                for(String dependecy: n.dependencies)
                {
                    //if the dependency isn't currently on the hashmap call dfs recursive. We only need to check if the value is present because our hashMap values
                    //are all initialized to null so the map either contains a value with <dependency_name,true> or it doesn't. The boolean value "false" is not needed 

                    if(!visited.containsKey(dependecy))
                    {        
                        dfs(dependecy,visited,stack);
                    }
                }
            }
        }
        //once finished exploring all the neighbours of our class, we push it to the stack.
        stack.push(start_class);
    }

    private void dfs(String start,HashMap<String,Boolean> visited,Set<String> comp,Set<node> reverse)
    {
        //put the name on the visited hashmap
        visited.put(start,true);
        //add the value of the current node name to the component
        comp.add(start);
        //rese of the code bellow is the same as in the other dfs but we aren't adding the values to the stack anymore.
        for(node n:reverse)
        {
            if(n.node_name.equals(start))
            {
                for(String dependecy: n.dependencies)
                {
                    if(!visited.containsKey(dependecy))
                    {
                        dfs(dependecy,visited,comp,reverse);
                    }
                }
            }
        }
    }

    public Set<node> reverse_graph()
    {
        Set<node> reverse=new HashSet<>();
        for(node n: adj_list)
        {
            for(String s: n.dependencies)
            {
                //try to find the node with the name s
                node found_node=find_node(s,adj_list);
                if(found_node!=null)
                {
                    //If found the node with the dependency name => 2 cases: node already exists in the reverse list or it doesnt.
                    node already_ex=find_node(s, reverse);
                    if(already_ex!=null)
                    {
                        //node already exists in the list; no need to add it.
                        already_ex.add_dependency(n.node_name);
                    }
                    else
                    {
                        //node doesn't exist in the reverse list yet; create a new node with the name of the dependency.
                        node helper_node=new node(s);
                        helper_node.add_dependency(n.node_name);
                        reverse.add(helper_node);
                    }
 
                }
            }
        }
        
        return reverse;
    }

    public node find_node(String name,Set<node> list)
    {
        for(node k:list)
        {
            if(k.node_name.equals(name))
                return k;
        }
        return null;
    }
}

class Client
{
    public static void main(String[] args)
    {
        direct_graph g=new direct_graph();
        // g.show_dependencies();
        System.out.println(g.Kosaraju());
    }
}